// Fill out your copyright notice in the Description page of Project Settings.

#include "Battery.h"
#include "Components/StaticMeshComponent.h"


ABattery::ABattery()
{
	GetMesh()->SetSimulatePhysics(true);
	//BatteryPower = 150.f;
}

void ABattery::WasCollected_Implementation()
{
	// Use the base pickup behavior
	Super::WasCollected_Implementation();

	if (destroyOnPickup)
	{
		// Destroy the battery
		Destroy();
	}
}

void ABattery::SetActive(bool newState)
{
	if (destroyOnPickup)
	{
		Super::SetActive(newState);
	}
}

// report the power level of the battery
float ABattery::GetPower()
{
	return BatteryPower;
}