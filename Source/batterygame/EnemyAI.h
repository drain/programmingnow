// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "EnemyAI.generated.h"

/**
 * 
 */
UCLASS()
class AEnemyAI : public AAIController 
{
	GENERATED_BODY()

	UPROPERTY(Transient)
	class UBlackboardComponent* BlackboardComp;

	UPROPERTY(Transient)
	class UBehaviorTreeComponent* BehaviorComp;


public:

	AEnemyAI();

	virtual void OnPossess(APawn* InPawn) override;

	uint8 EnemyKeyID;

	//~EnemyAI();
};
