// Fill out your copyright notice in the Description page of Project Settings.


#include "..\Public\BatterySaveGame.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

UBatterySaveGame::UBatterySaveGame()
{
	//LoadData();
}

void UBatterySaveGame::LoadData()
{
	//load data from file
	UBatterySaveGame* SaveGameInstance = Cast<UBatterySaveGame>(UGameplayStatics::CreateSaveGameObject(UBatterySaveGame::StaticClass()));
	SaveGameInstance = Cast<UBatterySaveGame>(UGameplayStatics::LoadGameFromSlot("highscore", 0));
	
	for (int i = 0; i < SaveGameInstance->highScores.Num(); i++)
	{
		this->highScores.Add(SaveGameInstance->highScores[i]);
	}

	this->highScores.Sort();
}

void UBatterySaveGame::SaveGame(float score)
{
	//add old highscores and the new one and save it
	UBatterySaveGame* saveGame = Cast<UBatterySaveGame>(UGameplayStatics::CreateSaveGameObject(UBatterySaveGame::StaticClass()));
	for (int i = 0; i < this->highScores.Num(); i++)
	{
		saveGame->highScores.Add(this->highScores[i]);
	}

	saveGame->highScores.Add(score);
	saveGame->highScores.Sort();

	UGameplayStatics::SaveGameToSlot(saveGame, TEXT("highscore"), 0);
}

void UBatterySaveGame::EraseData()
{
	//empty the array and write it ontop of the save file
	UBatterySaveGame* saveGame = Cast<UBatterySaveGame>(UGameplayStatics::CreateSaveGameObject(UBatterySaveGame::StaticClass()));
	this->highScores.Empty();
	saveGame->highScores = this->highScores;

	UGameplayStatics::SaveGameToSlot(saveGame, TEXT("highscore"), 0);
}
