// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Goal.generated.h"

/**
 * 
 */
UCLASS()
class BATTERYGAME_API AGoal : public ATriggerBox
{
	GENERATED_BODY()

public:
	AGoal();

	UFUNCTION()
	void OnOverlapBegin(class AActor* overlapped, class AActor* other);

	UFUNCTION()
	void OnOverlapEnd(class AActor* overlapped, class AActor* other);
};
