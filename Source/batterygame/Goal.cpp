// Fill out your copyright notice in the Description page of Project Settings.

#include "Goal.h"
#include "Engine/Engine.h"
#include "batterygameGameMode.h"
#include "batterygameCharacter.h"
#include "Public/BatterySaveGame.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

#define print(text) if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Red, text);

AGoal::AGoal()
{
	//bind functions to events
	this->OnActorBeginOverlap.AddDynamic(this, &AGoal::OnOverlapBegin);
	this->OnActorEndOverlap.AddDynamic(this, &AGoal::OnOverlapEnd);
}

void AGoal::OnOverlapBegin(class AActor* overlapped, class AActor* other)
{
	if (other && (other != this))
	{
		//print("Entered goal: " + overlapped->GetName());
		AbatterygameCharacter* player = Cast<AbatterygameCharacter>(other);

		//if player entered, win game!
		if (player)
		{
			AbatterygameGameMode* gameMode = Cast<AbatterygameGameMode>(GetWorld()->GetAuthGameMode());
			float finalTime = gameMode->GetCourseTimerSeconds();

			gameMode->StopCourseTimer();
			gameMode->SetCurrentState(EBatteryPlayState::EWon);

			//save final time to highscore
			UBatterySaveGame* savegame = Cast<UBatterySaveGame>(UGameplayStatics::CreateSaveGameObject(UBatterySaveGame::StaticClass()));
			
			savegame->LoadData();
			savegame->SaveGame(finalTime);
		}
	}
}

void AGoal::OnOverlapEnd(class AActor* overlapped, class AActor* other)
{
	if (other && (other != this))
	{
		//print("Left goal: " + overlapped->GetName());
	}
}