// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "batterygameGameMode.h"
#include "batterygameCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "SpawnVolume.h"
#include "Engine/World.h"
#include "Blueprint/UserWidget.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "TimerManager.h"
#include "Engine/Engine.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

int AbatterygameGameMode::s_tryCounter = 1;

AbatterygameGameMode::AbatterygameGameMode()
{
	PrimaryActorTick.bCanEverTick = true;

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	//base decay rate
	//DecayRate = 0.01f;
}

void AbatterygameGameMode::BeginPlay()
{
	Super::BeginPlay();

	// find all spawn volume Actors
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnVolume::StaticClass(), FoundActors);

	for (auto Actor : FoundActors)
	{
		ASpawnVolume* SpawnVolumeActor = Cast<ASpawnVolume>(Actor);
		if (SpawnVolumeActor)
		{
			SpawnVolumeActors.AddUnique(SpawnVolumeActor);
		}
	}

	SetCurrentState(EBatteryPlayState::EPlaying);

	// set the score to beat
	//this is set in bp
	/*AbatterygameCharacter* MyCharacter = Cast<AbatterygameCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MyCharacter)
	{
		MaxPower = (MyCharacter->GetInitialPower())*1.25f;
	}*/

	if (HUDWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}

	GetWorldTimerManager().ClearTimer(courseTimer);
	
	GetWorldTimerManager().SetTimer(courseTimer, this, &AbatterygameGameMode::ElapseTimer, 0.1f, true);

	//FTimerHandle Update;
	//GetWorldTimerManager().SetTimer(Update, this, &AbatterygameGameMode::Update, 0.01f, true);

}

//scrapped try at timer based update function
void AbatterygameGameMode::Update()
{
	// Check that we are using the battery collector character
	/*AbatterygameCharacter* MyCharacter = Cast<AbatterygameCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MyCharacter)
	{
		//if our character exicts collect batteries next to our character
		MyCharacter->CollectPickups();
		MyCharacter->KillEnemiesClose();

		if (GetCurrentState() != EBatteryPlayState::EWon)
		{
			if (MyCharacter->GetCurrentPower() > MaxPower)
			{
				//clamp power to max 2x the bar
				if (MyCharacter->GetCurrentPower() > (MaxPower * 2))
				{
					MyCharacter->SetPower(MaxPower * 2);
				}

				// decrease the character's power using the decay rate
				MyCharacter->UpdatePower(-0.1f * DecayRate*(MyCharacter->GetInitialPower()));
				MyCharacter->Overcharged = true;
			}
			else if (MyCharacter->GetCurrentPower() > 0)
			{
				// decrease the character's power using the decay rate
				MyCharacter->UpdatePower(-0.1f * DecayRate*(MyCharacter->GetInitialPower()));
				MyCharacter->Overcharged = false;
			}
			else
			{
				SetCurrentState(EBatteryPlayState::EGameOver);
			}
		}
	}*/
}

void AbatterygameGameMode::ElapseTimer()
{
	elapsedTimeSeconds += 0.1f;
}

float AbatterygameGameMode::GetCourseTimerSeconds()
{
	return elapsedTimeSeconds;
}

void AbatterygameGameMode::StopCourseTimer()
{
	GetWorldTimerManager().ClearTimer(courseTimer);
}

int AbatterygameGameMode::GetTryCounter()
{
	return s_tryCounter;
}

void AbatterygameGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Check that we are using the battery collector character
	AbatterygameCharacter* MyCharacter = Cast<AbatterygameCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MyCharacter)
	{
		//if our character exicts collect batteries next to our character
		MyCharacter->CollectPickups();
		//try to kill all enemies around us, if we have overcharge
		MyCharacter->KillEnemiesClose();

		//if we havent won, handle current power level
		if (GetCurrentState() != EBatteryPlayState::EWon)
		{
			if (MyCharacter->GetCurrentPower() > MaxPower)
			{
				// decrease the character's power using the decay rate
				MyCharacter->UpdatePower(-DeltaTime * DecayRate*(MyCharacter->GetInitialPower()));
				MyCharacter->Overcharged = true;

				//clamp power to max 1.5x the bar for overcharge
				if (MyCharacter->GetCurrentPower() > (MaxPower + MaxPower/2))
				{
					MyCharacter->SetPower(MaxPower + MaxPower/2);
				}
			}
			else if (MyCharacter->GetCurrentPower() > 0)
			{
				// decrease the character's power using the decay rate
				MyCharacter->UpdatePower(-DeltaTime * DecayRate*(MyCharacter->GetInitialPower()));
				MyCharacter->Overcharged = false;
			}
			else
			{
				if (!playDeadSoundOnce)
				{
					playDeadSoundOnce = true;
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), MyCharacter->DeathSound, MyCharacter->GetActorLocation());
				}

				SetCurrentState(EBatteryPlayState::EGameOver);
			}
		}
	}
}

float AbatterygameGameMode::GetPowerToWin() const
{
	return MaxPower;
}

EBatteryPlayState AbatterygameGameMode::GetCurrentState() const
{
	return CurrentState;
}

void AbatterygameGameMode::SetCurrentState(EBatteryPlayState NewState)
{
	//set current state
	CurrentState = NewState;
	// handle the new current state
	HandleNewState(CurrentState);

}

void AbatterygameGameMode::HandleNewState(EBatteryPlayState NewState)
{
	switch (NewState)
	{
		// If the game is playing
	case EBatteryPlayState::EPlaying:
	{
		// spawn volumes active
		for (ASpawnVolume* Volume : SpawnVolumeActors)
		{
			Volume->SetSpawningActive(true);
		}
	}
	break;
	// If we've won the game
	case EBatteryPlayState::EWon:
	{
		// spawn volumes inactive
		for (ASpawnVolume* Volume : SpawnVolumeActors)
		{
			Volume->SetSpawningActive(false);
		}

		int finalCounter = s_tryCounter;

		//TODO: Add try counter to save data.. probs not needed

		/*if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Green, TEXT("Total tries: " + FString::FromInt(finalCounter)));*/

		s_tryCounter = 1;
	}
	break;
	// If we've lost the game
	case EBatteryPlayState::EGameOver:
	{
		// spawn volumes inactive
		for (ASpawnVolume* Volume : SpawnVolumeActors)
		{
			Volume->SetSpawningActive(false);
		}
		// block player input
		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
		if (PlayerController)
		{
			PlayerController->SetCinematicMode(true, false, false, true, true);
		}
		// ragdoll the character
		AbatterygameCharacter* MyCharacter = Cast<AbatterygameCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));
		if (MyCharacter)
		{
			MyCharacter->GetMesh()->SetSimulatePhysics(true);
			MyCharacter->GetMovementComponent()->MovementState.bCanJump = false;
		}

	}
	break;
	// Unknown/default state
	default:
	case EBatteryPlayState::EUnknown:
	{
		// do nothing
	}
	break;
	}

}
