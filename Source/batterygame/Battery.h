// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "Battery.generated.h"

/**
 * 
 */
UCLASS()
class BATTERYGAME_API ABattery : public APickup
{
	GENERATED_BODY()
	
public:
	ABattery();

	/** Override the WasCollected function - use Implementation because it's a Blueprint Native Event */
	void WasCollected_Implementation() override;

	void SetActive(bool newState) override;

	float GetPower();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battery", Meta = (BlueprintProtected = "true"))
	float BatteryPower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battery", Meta = (BlueprintProtected = "true"))
	bool destroyOnPickup = true;

};
