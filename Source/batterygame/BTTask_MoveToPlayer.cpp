// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_MoveToPlayer.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "EnemyAI.h"
#include "batterygameCharacter.h"
#include "EnemyCharacter.h"
#include "Kismet/GameplayStatics.h"



EBTNodeResult::Type UBTTask_MoveToPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AEnemyAI* CharPC = Cast<AEnemyAI>(OwnerComp.GetAIOwner());

	AbatterygameCharacter *player = Cast<AbatterygameCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharPC->EnemyKeyID));

	AEnemyCharacter* thisAI = Cast<AEnemyCharacter>(CharPC->GetPawn());

	//check from the other blackboard if we can see the player
	bool IsSeeingEnemy = OwnerComp.GetBlackboardComponent()->GetValueAsBool("IsSeeingEnemy");

	float distanceToPlayer;
	float attackTimer = 1.5f;
	if(player)
		distanceToPlayer = (thisAI->GetActorLocation() - player->GetActorLocation()).Size();

	//Are we seeing enemy and are we far/close enough to start chasing
	if (IsSeeingEnemy && player && distanceToPlayer > (10) && distanceToPlayer < 4000)
	{
		//play detect sound once
		if (!thisAI->playedDetectSound)
		{
			thisAI->playedDetectSound = true;
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), thisAI->SpotSound, thisAI->GetActorLocation());
		}
		
		//move towards player
		thisAI->lastEnemyPos = player->GetActorLocation();
		CharPC->MoveToActor(player, 10, true, true, true, 0, true);
	}
	
	//are we close enough to hit the player
	//TODO: Should probs make this into its own task later
	if (IsSeeingEnemy && player && distanceToPlayer <= hitDistance && canAttack)
	{
		//call the event for blueprint
		thisAI->SpawnLightningEvent();

		canAttack = false;
		//attack player
		/*if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Emerald, TEXT("Attacking player"));*/

		TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
		FDamageEvent DamageEvent(ValidDamageTypeClass);

		//drain power from player
		player->UpdatePower(-thisAI->powerDrain);

		//player powerdown sound
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), thisAI->AttackSound, thisAI->GetActorLocation());

		//restart attack timer
		FTimerHandle UnusedHandle;
		GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &UBTTask_MoveToPlayer::AttackReady, attackTimer, false);
	}

	return EBTNodeResult::Succeeded;
}

void UBTTask_MoveToPlayer::AttackReady()
{
	canAttack = true;
}

