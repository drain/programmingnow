// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "BatterySaveGame.generated.h"

/**
 * 
 */
UCLASS()
class BATTERYGAME_API UBatterySaveGame : public USaveGame
{
	GENERATED_BODY()

	UBatterySaveGame();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Score)
	TArray<float> highScores;

	UFUNCTION(BlueprintCallable, Category = "SaveGame")
	void LoadData();
	
	void SaveGame(float score);

	UFUNCTION(BlueprintCallable, Category = "SaveGame")
	void EraseData();
};
