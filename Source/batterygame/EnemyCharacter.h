// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Sound/SoundCue.h"
#include "EnemyCharacter.generated.h"

UCLASS()
class AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

private:
	UPROPERTY(EditDefaultsOnly)
	float MaxHealth = 100;
	float CurHealth;


public:
	// Sets default values for this character's properties
	AEnemyCharacter();

	FVector lastEnemyPos;

	bool playedDetectSound;

	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* BotBehavior;

	UPROPERTY(EditDefaultsOnly)
	float powerDrain = 250;

	UPROPERTY(EditDefaultsOnly, Category = Sounds)
	USoundCue* AttackSound;

	UPROPERTY(EditDefaultsOnly, Category = Sounds)
	USoundCue* SpotSound;

	UPROPERTY(EditDefaultsOnly, Category = Sounds)
	USoundCue* DieSound;

	UFUNCTION(BlueprintImplementableEvent, Category = "Power")
	void SpawnLightningEvent();

	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) override;

};
