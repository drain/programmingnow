// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "batterygameCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Pickup.h"
#include "Battery.h"
#include "Classes/Components/SphereComponent.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "batterygameGameMode.h"
#include "EnemyCharacter.h"
#include "GameFramework/DamageType.h"
//#include "Engine/Engine.h"

//////////////////////////////////////////////////////////////////////////
// AbatterygameCharacter

AbatterygameCharacter::AbatterygameCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create the collection sphere
	CollectionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollectionSphere"));
	CollectionSphere->AttachTo(RootComponent);
	CollectionSphere->SetSphereRadius(200.f);

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	//set a base power level for the character
	InitialPower = 2000.f;
	CharacterPower = InitialPower;

	// set the dependence of the speed on the power level
	SpeedFactor = 0.75f;
	BaseSpeed = 10.0f;


}

//////////////////////////////////////////////////////////////////////////
// Input

void AbatterygameCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AbatterygameCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	//PlayerInputComponent->BindAction("Collect", IE_Released, this, &AbatterygameCharacter::CollectPickups);
	PlayerInputComponent->BindAction("ReloadLevel", IE_Released, this, &AbatterygameCharacter::CallReloadLevel);
	PlayerInputComponent->BindAction("BackToMenu", IE_Released, this, &AbatterygameCharacter::CallBackToMenu);

	PlayerInputComponent->BindAxis("MoveForward", this, &AbatterygameCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AbatterygameCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AbatterygameCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AbatterygameCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AbatterygameCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AbatterygameCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AbatterygameCharacter::OnResetVR);
}

void AbatterygameCharacter::Jump()
{
	Super::Jump();
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), JumpSound, GetActorLocation());
}

void AbatterygameCharacter::CallReloadLevel()
{
	AbatterygameGameMode::s_tryCounter++;
	UGameplayStatics::OpenLevel(this, "TestMap");
}

void AbatterygameCharacter::CallBackToMenu()
{
	UGameplayStatics::OpenLevel(this, "MainMenu");
}

void AbatterygameCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AbatterygameCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AbatterygameCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AbatterygameCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AbatterygameCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AbatterygameCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AbatterygameCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AbatterygameCharacter::CollectPickups()
{
	// Get all overlapping Actors and store them in an array
	TArray<AActor*> CollectedActors;
	CollectionSphere->GetOverlappingActors(CollectedActors);

	// keep track of the collected battery power
	float CollectedPower = 0;

	// For each Actor we collected
	for (int32 iCollected = 0; iCollected < CollectedActors.Num(); ++iCollected)
	{
		// Cast the actor to APickup
		APickup* const TestPickup = Cast<APickup>(CollectedActors[iCollected]);
		// If the cast is successful and the pickup is valid and active 
		if (TestPickup && !TestPickup->IsPendingKill() && TestPickup->IsActive())
		{
			// Call the pickup's WasCollected function
			TestPickup->WasCollected();
			// Check to see if the pickup is also a battery
			ABattery* const TestBattery = Cast<ABattery>(TestPickup);
			if (TestBattery)
			{
				//GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Red, "Picking up battery!");
				// increase the collected power

				if(TestBattery->GetPower() > 0)
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), PowerUp, GetActorLocation());
				else if(TestBattery->GetPower() < 0)
					UGameplayStatics::PlaySoundAtLocation(GetWorld(), PowerDown, GetActorLocation());

				CollectedPower += TestBattery->GetPower();
				TestBattery->SetActive(false);
			}
			else
				// Deactivate the pickup 
				TestPickup->SetActive(false);
		}
	}
	if (CollectedPower != 0)
	{
		UpdatePower(CollectedPower);
	}
}

void AbatterygameCharacter::KillEnemiesClose()
{
	if (Overcharged)
	{
		TArray<AActor*> EnemiesNear;
		CollectionSphere->GetOverlappingActors(EnemiesNear);

		for (int32 iCollected = 0; iCollected < EnemiesNear.Num(); ++iCollected)
		{
			// Cast the actor to enemy
			AEnemyCharacter* const enemy = Cast<AEnemyCharacter>(EnemiesNear[iCollected]);
			// If the cast is successful
			if (enemy && !enemy->IsPendingKill())
			{
				//https://docs.unrealengine.com/en-US/API/Runtime/Engine/GameFramework/AActor/TakeDamage/index.html
				TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
				FDamageEvent DamageEvent(ValidDamageTypeClass);

				// deal dmg to enemy
				enemy->TakeDamage(200, DamageEvent, enemy->GetController(), this);
			}
		}
	}
}

// Reports starting power
float AbatterygameCharacter::GetInitialPower()
{
	return InitialPower;
}

// Reports current power
float AbatterygameCharacter::GetCurrentPower()
{
	return CharacterPower;
}

// called whenever power is increased or decreased
void AbatterygameCharacter::UpdatePower(float PowerChange)
{
	// change power
	CharacterPower = CharacterPower + PowerChange;
	// change speed based on power
	GetCharacterMovement()->MaxWalkSpeed = BaseSpeed + SpeedFactor * CharacterPower;
	// call visual effect
	PowerChangeEffect();
}

// Set player power
void AbatterygameCharacter::SetPower(float powerToSet)
{
	this->CharacterPower = powerToSet;
	GetCharacterMovement()->MaxWalkSpeed = BaseSpeed + SpeedFactor * CharacterPower;
	PowerChangeEffect();
}
